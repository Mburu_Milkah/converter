package com.example.milkah.myapplication;

import android.renderscript.Double2;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
public class MainActivity extends AppCompatActivity {
    Button btn1,btn2;
    EditText  txt1,txt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt1=(EditText)findViewById(R.id.txt1);
        btn1= (Button) findViewById(R.id.convert_btn);
        btn2= (Button) findViewById(R.id.exit_btn);
        txt2=(EditText)findViewById(R.id.txt2);
        btn1.setOnClickListener(new View.OnClickListener() {

                                    public void onClick(View view) {
                                        Double milimeter = Double.parseDouble(txt1.getText().toString());
                                        Double result;
                                        result = (milimeter / 25.4);
                                        txt2.setText(Double.toString(result));

                                    }
                                }
        );
        btn2.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view){
                finish();
            }
        });
    }
}
